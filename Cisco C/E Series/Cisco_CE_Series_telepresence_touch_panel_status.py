#!/usr/local/bin/python2.7

#---------------------------------------------------------------------------------------------------
# Name:         Cisco C/E Series Touch Panel Status
# Purpose:      Dynamic app to collect and store status of Cisco C/E Series room system touch panels
#
# Author:       jbaltes
#
# Created:      02.11.2014
# Revision:     03.20.2014
# Copyright:    None
# License:      
#---------------------------------------------------------------------------------------------------
# Modification Log :
# Ver No    Date            Author          Change Details
#---------------------------------------------------------------------------------------------------
# 0.01A     02.11.2014      Joe Baltes      Initial Version logic stub
# 0.01B     02.12.2014      Joe Baltes      Added function to handle the jacked up TBG C/E ssh shell
# 0.01C     03.20.2014      Joe Baltes      Added functions to grab mic levels
#---------------------------------------------------------------------------------------------------

#~ SSH Output example output from the Tandberg
#~ 
#~ xstatus Experimental Peripherals ConnectedDevice
#~ *s Experimental Peripherals ConnectedDevice 1001 Type: TouchPanel
#~ *s Experimental Peripherals ConnectedDevice 1001 Name: "Cisco TelePresence Touch"
#~ *s Experimental Peripherals ConnectedDevice 1001 ID: "00:50:60:05:48:cd"
#~ *s Experimental Peripherals ConnectedDevice 1001 Status: Connected
#~ *s Experimental Peripherals ConnectedDevice 1001 SoftwareInfo: "TT6.0.1 65adebe"
#~ *s Experimental Peripherals ConnectedDevice 1001 HardwareInfo: "101650"
#~ ** end
#~ 
#~ OK

import time
import paramiko

#~ set up the basic variables
#~ for dynamic app use on collector
host = self.cred_details['cred_host']
user = self.cred_details['cred_user']
passwd = self.cred_details['cred_pwd']
port = self.cred_details['cred_port']
timeout = self.cred_details['cred_timeout']

command = 'xstatus Experimental Peripherals ConnectedDevice'
command2 = 'xConfiguration Audio Input Microphone'

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect(host, username=user, password=passwd, port=int(port), timeout=float(timeout))

#~ function to handle passing commands to the remote shell
def tbgShell(host,port,username,password,cmd):
    """send an arbitrary command to a Cisco/TBG gizmo's ssh and 
    get the result"""
    transport = paramiko.Transport((host, port))
    #~ transport.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    transport.connect(username = user, password = passwd)
    chan = transport.open_channel("session")
    chan.setblocking(0)
    chan.invoke_shell()
    
    out = ''
    
    chan.send(cmd+'\n')
    
    tCheck = 0
    
    while not chan.recv_ready():
        time.sleep(1)
        tCheck+=1
        if tCheck >= 6:
            print 'time out'#TODO: add exeption here
            return False
    out = chan.recv(1024)
    
    return out

start_time = time.time()

output = tbgShell(host, port, user, passwd, command)

output = output.split('\n')

paneltype = []
panelname = []
panelid = []
panelstatus = []
softwareinfo = []
hardwareinfo = []


for line in output:
    if '1001 Type' in line:
        paneltype.append((0, line.split(': ')[1].strip() )) 
    if '1001 Name' in line:
        panelname.append((0, line.split(': ')[1].strip() ))
    if '1001 ID' in line:
        panelid.append((0, line.split(': ')[1].strip() ))
    if '1001 Status' in line:
        panelstatus.append((0, line.split(': ')[1].strip() ))
    if '1001 SoftwareInfo' in line:
        softwareinfo.append((0, line.split(': ')[1].strip() ))
    if '1001 HardwareInfo' in line:
        hardwareinfo.append((0, line.split(': ')[1].strip() ))
        
output2 = tbgShell(host, port, user, passwd, command2)

output2 = output2.split('\n')

mic1Level = []
mic2Level = []
mic3Level = []
mic4Level = []


for line in output2:
    if 'Microphone 1 Level:' in line:
        mic1Level.append((0, line.split(': ')[1].strip() )) 
    if 'Microphone 2 Level:' in line:
        mic2Level.append((0, line.split(': ')[1].strip() ))
    if 'Microphone 3 Level:' in line:
        mic3Level.append((0, line.split(': ')[1].strip() ))
    if 'Microphone 4 Level:' in line:
        mic4Level.append((0, line.split(': ')[1].strip() ))
        

elapsed_time = time.time() - start_time

for group, oid_group in self.oids.iteritems():
    for obj_id, oid_detail in oid_group.iteritems():
        if oid_detail['oid_type'] != snippet_id:
            # This collection object is obtained from a different Snippet
            continue
        
        oid = oid_detail["oid"]
        if oid == 'paneltype' and len(paneltype) > 0:
            oid_detail["result"] = paneltype
            oid_detail["oid_time"] = elapsed_time
        elif oid == 'panelname' and len(panelname) > 0:
            oid_detail["result"] = panelname
            oid_detail["oid_time"] = elapsed_time
        elif oid == 'panelid' and len(panelid) > 0:
            oid_detail["result"] = panelid
            oid_detail["oid_time"] = elapsed_time
        elif oid == 'panelstatus' and len(panelstatus) > 0:
            oid_detail["result"] = panelstatus
            oid_detail["oid_time"] = elapsed_time
        elif oid == 'softwareinfo' and len(softwareinfo) > 0:
            oid_detail["result"] = softwareinfo
            oid_detail["oid_time"] = elapsed_time
        elif oid == 'hardwareinfo' and len(hardwareinfo) > 0:
            oid_detail["result"] = hardwareinfo
            oid_detail["oid_time"] = elapsed_time
        elif oid == 'mic1Level' and len(mic1Level) > 0:
            oid_detail["result"] = mic1Level
            oid_detail["oid_time"] = elapsed_time
        elif oid == 'mic2Level' and len(mic2Level) > 0:
            oid_detail["result"] = mic2Level
            oid_detail["oid_time"] = elapsed_time
        elif oid == 'mic3Level' and len(mic3Level) > 0:
            oid_detail["result"] = mic3Level
            oid_detail["oid_time"] = elapsed_time
        elif oid == 'mic4Level' and len(mic4Level) > 0:
            oid_detail["result"] = mic4Level
            oid_detail["oid_time"] = elapsed_time
