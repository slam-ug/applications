# Applications
This project contains dynamic applications for different technologies, seggregated per Power Pack.

## Layout
The structure in this project is the following:
  * Project root
	* Vendor (e.g. Cisco, Microsoft, HPEnterprise)
	  * Hardware
	    * 'Device' (e.g. ISE5000, blade enclosure, ASA5500)
	  * Software
	    * 'Application' (e.g. SQL Server, Database, Exchange, Azure)